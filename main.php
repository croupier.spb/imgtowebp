<?php
$path = '/path/to/img/folder/';
$imgExtensions = [
    'jpg',
    'jpeg',
    'png',
];
$cwebpOptions = '-m 6 -z 9 -q 75';

$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));

foreach ($iterator as $key => $value) {
    $data = [];

    if ($value->isFile()) {
        $extension = $value->getExtension();
        $realPath = $value->getRealPath();

        if (in_array($extension, $imgExtensions)) {
            $pathSource = escapeshellarg($realPath);
            $pathTarget = escapeshellarg(
                preg_replace(
                    '@\.' . $extension . '$@',
                    '.webp',
                    $realPath
                )
            );
            $cmd = "cwebp $cwebpOptions $pathSource -o $pathTarget";
            exec($cmd);
            print "\n\n$pathSource\n$pathTarget\n\n";
        }
    }
}